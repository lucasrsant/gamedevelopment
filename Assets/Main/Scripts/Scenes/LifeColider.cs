﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LifeColider : MonoBehaviour {

	public float lifePercent;

	GameObject player;
	PlayerHealth playerHealth;
	bool playerInRange;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent <PlayerHealth> ();	
	}
	
	void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = true;
        }
    }


    void OnTriggerExit (Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = false;
        }
    }

	// Update is called once per frame
	void Update () 
	{
		if(!playerInRange || playerHealth.currentHealth <= 0)
			return;

		playerHealth.AddLife(Convert.ToInt32(playerHealth.startingHealth * lifePercent));
		Destroy(this.gameObject);
		HeartManager.hearts--;
	}
}
