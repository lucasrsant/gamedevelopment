﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentController : MonoBehaviour {

    public AudioClip footsteps;

    [SerializeField]
    private Camera cam;

    private Animator anim;
    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;
    private Vector3 thrusterForce = Vector3.zero;
    public float speed = 2.0f;

    [SerializeField]
    private float cameraRotationLimit = 85f;

    private Rigidbody rb;
    AudioSource playerAudio;
    float TimeToPlayAudio = 0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float zMove = Input.GetAxis("Vertical");
        float xMove = Input.GetAxis("Horizontal");
        Vector3 _movHorizontal = transform.right * xMove;
        Vector3 _movVertical = transform.forward * zMove;    

        Animating(xMove, zMove);

        // Anda
        Vector3 _velocity = (_movHorizontal + _movVertical) * speed;
        Move(_velocity);

        // Rotaciona camera para ajustar direcional
        float _yRot = Input.GetAxisRaw("Mouse X");
        Vector3 _rotation = new Vector3(0f, _yRot, 0f);
        Rotate(_rotation);

        // Segundo ajuste de camera para mudança eixo y
        float _xRot = Input.GetAxisRaw("Mouse Y");
        float _cameraRotationX = _xRot;
        RotateCamera(_cameraRotationX);
    }
    
    // Gets a movement vector
    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    // Gets a rotational vector
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }

    // Gets a rotational vector for the camera
    public void RotateCamera(float _cameraRotationX)
    {
        cameraRotationX = _cameraRotationX;
    }

    // Get a force vector for our thrusters
    public void ApplyThruster(Vector3 _thrusterForce)
    {
        thrusterForce = _thrusterForce;
    }

    // Run every physics iteration
    void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }

    //Perform movement based on velocity variable
    void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }

        if (thrusterForce != Vector3.zero)
        {
            rb.AddForce(thrusterForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        }

    }

    void Animating(float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("IsWalking", walking);
        TimeToPlayAudio = walking ? TimeToPlayAudio += Time.deltaTime : 0;

        if (walking && TimeToPlayAudio >= 0.4)
        {
            TimeToPlayAudio = 0f;
            playerAudio.clip = footsteps;
            playerAudio.volume = .1f;
            playerAudio.Play();
        }
    }

    //Perform rotation
    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null)
        {
            // Set our rotation and clamp it
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

            //Apply our rotation to the transform of our camera
            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
        }
    }
}
