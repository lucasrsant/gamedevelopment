﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public static int lvl;
    public GameObject xpPoints;
    public GameObject lvlPoints;
    public GameObject scorePoints;
    public int multipleLvl;

    Text _xpPointsText;
    Text _xpLvlText;
    Text _scorePointsText;

    void Awake ()
    {
        _xpPointsText = xpPoints.GetComponent<Text> ();
        _xpLvlText = lvlPoints.GetComponent<Text>();
        _scorePointsText = scorePoints.GetComponent<Text>();
        score = 0;
        EnemyManager.qtdMonsters++;
        KillMonster(0);
    }

    public static void KillMonster(int xpMonster)
    {
        EnemyManager.qtdMonsters--;
        score+=xpMonster;

        if (EnemyManager.qtdMonsters == 0)
        {
            lvl++;
            EnemyManager.lvlUp = true;
        }
    }

    void Update ()
    {
        _xpPointsText.text = "Monsters Left: " + EnemyManager.qtdMonsters;
        _xpLvlText.text = "Level: " + lvl;
        _scorePointsText.text = "Score: " + score;
    }
}
