﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartManager : MonoBehaviour {

	public PlayerHealth playerHealth;    
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    public GameObject heart;

    public static int hearts;

    void Start ()
    {
    	hearts = 0;
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn ()
    {
        if (playerHealth.currentHealth <= 0f || hearts == 2)
            return;

        while(hearts != 2)
        {
            int spawnPointIndex = Random.Range(0, spawnPoints.Length);
            Instantiate(heart, spawnPoints[spawnPointIndex].position, Quaternion.Euler(-84, -36, 123));

            hearts++;	
        }
    }
}
