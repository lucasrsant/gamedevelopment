﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;    
    public float spawnTime = 3f;
    public int multiInLvl = 5;
    public Transform[] spawnPoints;
    public GameObject zombunny;
    public GameObject zombear;
    public GameObject hellephant;

    public static int qtdMonsters;
    public static bool lvlUp;
    public static int enemysToRespawn;

    private int monsterToInvoke;

    void Start ()
    {
        monsterToInvoke = 0;
        qtdMonsters = 0;
        enemysToRespawn = 0;
    }

    void Update()
    {
        if(lvlUp)
        {
            monsterToInvoke = multiInLvl * ScoreManager.lvl;
            qtdMonsters = multiInLvl * ScoreManager.lvl;
            InvokeRepeating("Spawn", 1, 1);
            lvlUp = false;
        }
    }

    void Spawn ()
    {
        if (playerHealth.currentHealth <= 0f)
            return;

        GameObject monsterToSpawn;
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // Sorted monster, using probabl to zombanny > zombear > hellephant
        int enemyIndex = Random.Range(0, 17);
        monsterToSpawn = enemyIndex <= 10 ? zombunny : (enemyIndex <= 15 ? zombear : hellephant);
        Instantiate(monsterToSpawn, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        monsterToInvoke--;

        if(monsterToInvoke == 0)
            CancelInvoke("Spawn");
    }
}
