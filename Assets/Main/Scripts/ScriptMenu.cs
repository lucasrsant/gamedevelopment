﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptMenu : MonoBehaviour {

	public void PlayGame()
	{
        SceneManager.LoadScene(1);
    }

    public void RestartGame()
    {
        PlayGame();
        ScoreManager.score = 0;
        ScoreManager.lvl = 1;
        EnemyManager.lvlUp = true;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
