﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;

public class HighScores
{
    public string _name { get; set; }
    public int _score { get; set; }
}

public class HighScoreManager : MonoBehaviour {

    public GameObject NamesObject;
    public GameObject ScoreObject;
    public GameObject InputPlayer;
    public GameObject InputText;

    Text _namesText;
    Text _scoreText;
    Text _inputText;

    // Use this for initialization
    void Start() {
        _namesText = NamesObject.GetComponent<Text>();
        _scoreText = ScoreObject.GetComponent<Text>();
        _inputText = InputText.GetComponent<Text>();
        
        CheckNewHighScore();
    }

    void CheckNewHighScore()
    {
        var currentScore = PlayerPrefs.GetInt("CurrentScore", 0);
        var newScore = false;

        for (int i = 0; i < 10; i++)
        {
            if(currentScore > PlayerPrefs.GetInt("HighScoreNumber" + i, 0))
            {
                newScore = true;
                break;
            }
        }

        if (newScore)
            InputPlayer.SetActive(true);
        else
            CheckScore();
    }

    public void CheckScore()
    {
        InputPlayer.SetActive(false);

        var currentScore = PlayerPrefs.GetInt("CurrentScore", 0);
        List<HighScores> scores = new List<HighScores>();

        // Get current scores
        for (int i = 0; i < 10; i++)
        {
            scores.Add(new HighScores() {
                _score = PlayerPrefs.GetInt("HighScoreNumber" + i, 0),
                _name = PlayerPrefs.GetString("HighScoreNames" + i, "")
            });
        }

        scores = scores.OrderByDescending(x => x._score).ToList();

        // Check if current score is bigger than others
        int scorePosition = -1;
        for (int i = 0; i < 10; i++)
        {
            if(currentScore > scores[i]._score)
            {
                scorePosition = i;
                break;
            }
        }

        // Update scores
        if(scorePosition != -1)
        {
            for (int i = 9; i >= 0; i--)
            {
                if (i == scorePosition)
                {
                    if (i != 9)
                        scores[i + 1] = scores[i];

                    scores[i] = new HighScores()
                    {
                        _name = _inputText.text,
                        _score = currentScore
                    };

                    break;
                }
                else if(i != 9)
                    scores[i + 1] = scores[i];
            }
        }

        // Write scores in player prefs
        for(int i = 0; i < 10; i++)
        {
            PlayerPrefs.SetInt("HighScoreNumber" + i, scores[i]._score);
            PlayerPrefs.SetString("HighScoreNames" + i, scores[i]._name);
        }

        PlayerPrefs.Save();

        _namesText.text = "";
        _scoreText.text = "";
        for (int i = 0; i < 10; i++)
        {
            _namesText.text += ((i + 1) < 10 ? "0" + (i + 1).ToString() : (i + 1).ToString()) + "  -  " + PlayerPrefs.GetString("HighScoreNames" + i, "") + "\n";
            _scoreText.text += PlayerPrefs.GetInt("HighScoreNumber" + i, 0).ToString() + "\n";
        }
    }
}
