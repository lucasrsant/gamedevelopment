﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {
	
	public Transform _canvas;
    public Transform _player;
    public Transform _pointer;

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape) && !_canvas.gameObject.activeInHierarchy) 
		{
            _pointer.gameObject.SetActive(false);
            _canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
            _player.GetComponent<movimentController>().enabled = false;
            _player.GetComponent<cameraController>().enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
	}

    public void Resume()
    {
        _pointer.gameObject.SetActive(true);
        _canvas.gameObject.SetActive(false);
        Time.timeScale = 1;
        _player.GetComponent<movimentController>().enabled = true;
        _player.GetComponent<cameraController>().enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
